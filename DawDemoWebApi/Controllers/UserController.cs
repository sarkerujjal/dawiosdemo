﻿using DAW.BLL;
using DAW.Entites;
using DAW.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DawDemoWebApi.Controllers
{
    public class UserController : ApiController
    {
        private readonly IUserService _userService;
        //private readonly IJsonFeedService _jsonFeedService;
        //private readonly Users _user;
        public UserController()
        {
            //_user = new Users();
            //_user = AuthenticationController.User();
            _userService = new UserBLL();
            //_jsonFeedService = new JsonFeedBLL();
            //_dawAdminService = new DawAdminBLL();
        }

        [HttpPost]
        public IHttpActionResult Create(UserEntities user)
        {
            int result = 0;
            if(!string.IsNullOrEmpty(user.Firstname) && user.Systemid>0)
            {
                result = _userService.CreateNewUser(user);
            }

            return Json(result);
        }

        [HttpGet]
        public IHttpActionResult Details(int id)
        {
            var result = new UserEntities();
            if(id>0)
            {
                result = _userService.GetUserById(id,1);// for now only one system id
            }

            return Json(result);
        }
        [HttpPost]
        public IHttpActionResult Edit(UserEntities user)
        {
            int result = 0;
            if (user.UserId > 0 )
            {
                user.Systemid = 1;// for now only one system id 
                result = _userService.UpdateUserById(user);
            }

            return Json(result);
        }
        [HttpPost]
        public IHttpActionResult Delete(int id)
        {
            var result = 0;
            if (id > 0)
            {
                result = _userService.DeleteUserById(id, 1);// for now only one system id
            }

            return Json(result);
        }
        [HttpGet]
        public IHttpActionResult Users()
        {
            return Json(_userService.GetUsersBySystemId(1));//now we have only one System's user list 
        }
    }
}
