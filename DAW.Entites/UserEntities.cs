﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAW.Entites
{
    public class UserEntities
    {
        public int UserId { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public DateTime Dateofbirth { get; set; }
        public string Address { get; set; }
        public string Address2 { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Email1 { get; set; }
        public string Email2 { get; set; }
        public int Systemid { get; set; }
        public decimal Weightkg { get; set; }
        public string Imageurl { get; set; }
        public string Lastweightedby { get; set; }
        public DateTime Lastweighteddate { get; set; }
        public bool Active { get; set; }
        public DateTime Activedate { get; set; }
        public DateTime Inactivedate { get; set; }
        public int Createdby { get; set; }
        public DateTime Createddate { get; set; }
        public int Lastupdateby { get; set; }
        public DateTime Lastupdatedate { get; set; }
    }
}
