﻿
using DAW.Entites;
using DAW.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAW.BLL
{
    public class UserBLL : IUserService
    {
        private readonly IUserService _iUserService;
        public UserBLL()
        {
            _iUserService = new DAW.DAL.UserDAL();
        }

        #region CreateNewUser
        public int CreateNewUser(UserEntities user)
        {
            return _iUserService.CreateNewUser(user);
        }
        #endregion

        #region GetUserById
        public UserEntities GetUserById(int userid, int systemid)
        {
            return _iUserService.GetUserById(userid, systemid);
        }
        #endregion
        #region UpdateUserById 
        public int UpdateUserById(UserEntities user)
        {
            return _iUserService.UpdateUserById(user);
        }
        #endregion

        #region  DeleteUserById
        public int DeleteUserById(int userid, int systemid)
        {
            return _iUserService.DeleteUserById(userid, systemid);
        }
        #endregion

        
        #region GetUserBySystemId
        public List<UserEntities> GetUsersBySystemId(int systemid)
        {
            return _iUserService.GetUsersBySystemId(systemid);
        }
        #endregion

    }
}
