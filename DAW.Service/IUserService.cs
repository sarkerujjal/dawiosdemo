﻿using DAW.Entites;
using System;
using System.Collections.Generic;


namespace DAW.Service
{
    public interface IUserService
    {
        int CreateNewUser(UserEntities user);
        int UpdateUserById(UserEntities user);
        UserEntities GetUserById(int userid, int systemid);
        int DeleteUserById(int userid, int systemid);
        List<UserEntities> GetUsersBySystemId(int systemid);

    }
}
