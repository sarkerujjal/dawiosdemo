﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics;

namespace DAW.DAL
{
    class DatabaseDAL
    {
        private static readonly Database Database = DAWAppConfiguration.SQLAppConnectionManager;
        private static readonly string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DAWConnection"].ConnectionString;

        public static IDataReader QueryReader(string sql, bool isStoredProcedure, SqlParameter[] addParameters)
        {
            //QueryReader
            //
            //var parameters = new[]{
            //    new SqlParameter(){ ParameterName="@userId", Value=190}
            //};
            //using (var reader = DatabaseDAL.QueryReader(string.Format(@"Select * From Memberdata Where ID = @userId"), false, parameters))
            //{
            //    while (reader.Read())
            //    {
            //        Int32 bradId = reader["ID"] == DBNull.Value ? 0 : int.Parse(reader["ID"].ToString());
            //        var bradId2 = bradId;
            //    }
            //    reader.Dispose();
            //}
            //


            IDataReader reader;
            using (var dbConnection = Database.CreateConnection())
            {
                using (DbCommand dbCommand = isStoredProcedure == false ? Database.GetSqlStringCommand(sql) : Database.GetStoredProcCommand(sql))
                {

                    foreach (var param in addParameters)
                    {
                        Database.AddInParameter(dbCommand, param.ParameterName, param.DbType, param.Value);
                    }
                    try
                    {
                        Debug.WriteLine("Executing QueryReader -> " + sql);
                        dbConnection.Open();
                        reader = Database.ExecuteReader(dbCommand);
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine("ERROR Running QueryReader ->" + sql);
                        Debug.WriteLine(ex.Message);
                        foreach (var param in addParameters)
                        {
                            Debug.WriteLine("Parameter: " + param.ParameterName + " = " + param.Value + " [" + param.DbType + "]");
                        }
                        throw new Exception(ex.Message);
                    }
                    finally
                    {
                        dbCommand.Dispose();
                        dbConnection.Close();
                    }
                }
            }
            return reader;
        }

        //wirhout input parameter 

        public static IDataReader QueryReader(string sql, bool isStoredProcedure)
        {

            IDataReader reader;
            using (var dbConnection = Database.CreateConnection())
            {
                using (DbCommand dbCommand = isStoredProcedure == false ? Database.GetSqlStringCommand(sql) : Database.GetStoredProcCommand(sql))
                {
                    try
                    {
                        Debug.WriteLine("Executing QueryReader -> " + sql);
                        dbConnection.Open();
                        reader = Database.ExecuteReader(dbCommand);
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine("ERROR Running QueryReader ->" + sql);
                        Debug.WriteLine(ex.Message);
                        throw new Exception(ex.Message);
                    }
                    finally
                    {
                        dbCommand.Dispose();
                        dbConnection.Close();
                    }
                }
            }
            return reader;
        }

        public static bool QueryNonReader(string sql, bool isStoredProcedure, SqlParameter[] addParameters)
        {
            //QueryNonReader:
            //
            //var parameters2 = new[]{
            //    new SqlParameter(){ ParameterName="@userId", Value=0}
            //};
            //DatabaseDAL.QueryNonReader(string.Format(@"Delete From Memberdata Where ID = @userId"), false, parameters);

            bool success = true;
            using (var dbConnection = Database.CreateConnection())
            {
                using (DbCommand dbCommand = isStoredProcedure == false ? Database.GetSqlStringCommand(sql) : Database.GetStoredProcCommand(sql))
                {
                    foreach (var param in addParameters)
                    {
                        Database.AddInParameter(dbCommand, param.ParameterName, param.DbType, param.Value);
                    }
                    try
                    {
                        Debug.WriteLine("Executing QueryNonReader -> " + sql);
                        dbConnection.Open();
                        Database.ExecuteNonQuery(dbCommand);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                    finally
                    {
                        dbCommand.Dispose();
                        dbConnection.Close();
                    }
                }
            }
            return success;
        }

        public static void QueryNonReader(string sql, bool isStoredProcedure, List<SqlParameter[]> addParameters)
        {
            //Usage:
            //List<SqlParameter[]> addParameters = new List<SqlParameter[]>();
            ////Command #1
            //addParameters.Add(new[]
            //{
            //    new SqlParameter() {ParameterName = "@Value", Value = "Brad"}
            //});
            ////Command #2
            //addParameters.Add(new[]
            //{
            //    new SqlParameter() {ParameterName = "@Value", Value = "Brad2"}
            //});

            ////Run Insert
            //DatabaseDAL.QueryNonReader(String.Format(@"Insert Into ServiceTables 
            //    (SystemID,ServiceTableName,ServiceVariableName,ServiceVariableValue,ServiceVariableNumber,ServiceVariableLastUpdated,ServiceVariableLastUpdatedBy,Time_Zone_Adjust,Status_Code,Status_Message,Status_Changed_By)
            //    Values (1,@Value,@Value,@Value,'0','2019-01-01',Null,0,Null,Null,Null);"), false, addParameters);

            using (var dbConnection = Database.CreateConnection())
            {
                try
                {
                    dbConnection.Open();
                    foreach (var pList in addParameters)
                    {
                        using (DbCommand dbCommand = isStoredProcedure == false ? Database.GetSqlStringCommand(sql) : Database.GetStoredProcCommand(sql))
                        {
                            foreach (SqlParameter p in pList)
                            {
                                Database.AddInParameter(dbCommand, p.ParameterName, p.DbType, p.Value);
                            }
                            try
                            {
                                Debug.WriteLine("Executing QueryNonReader -> " + sql);
                                Database.ExecuteNonQuery(dbCommand);
                            }
                            catch (Exception ex)
                            {
                                throw new Exception(ex.Message);
                            }
                            finally
                            {
                                dbCommand.Dispose();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    dbConnection.Close();
                }
            }
        }



        public static SqlParameter[] QueryNonReaderOutputParameters(string sql, SqlParameter[] addParameters, SqlParameter[] outParameters)
        {
            //QueryNonReaderOutputParameters:
            //var parametersIn = new[]{
            //    new SqlParameter(){ ParameterName="@varTicket", Value=1},
            //    new SqlParameter(){ ParameterName="@varSystem", Value=32},
            //    new SqlParameter(){ ParameterName="@varGroup", Value=0},
            //    new SqlParameter(){ ParameterName="@varUser", Value=190},
            //    new SqlParameter(){ ParameterName="@varBase", Value=0},
            //    new SqlParameter(){ ParameterName="@varPriority", Value=0},
            //    new SqlParameter(){ ParameterName="@varSubject", Value="Bradley!!"},
            //    new SqlParameter(){ ParameterName="@varDescription", Value="Message!!"},
            //    new SqlParameter(){ ParameterName="@varBrowser_Information", Value="Browser Info"}
            //};
            //var parametersOut = new[]{
            //    new SqlParameter(){ ParameterName="@varTicket"}
            //};
            //SqlParameter[] bg = DatabaseDAL.QueryNonReaderOutputParameters("sp_tickets_new", parametersIn, parametersOut);
            // save = (int)bg[0].Value;
            List<SqlParameter> parameterList = new List<SqlParameter>();
            using (var dbConnection = Database.CreateConnection())
            {
                using (DbCommand dbCommand = Database.GetStoredProcCommand(sql))
                {
                    foreach (var param in addParameters)
                    {
                        Database.AddInParameter(dbCommand, param.ParameterName, param.DbType, param.Value);
                    }
                    foreach (var param in outParameters)
                    {
                        Database.AddOutParameter(dbCommand, param.ParameterName, param.DbType, param.Size);
                    }
                    try
                    {
                        Debug.WriteLine("Executing QueryNonReaderOutputParameters -> " + sql);
                        dbConnection.Open();
                        Database.ExecuteNonQuery(dbCommand);
                        foreach (var param in outParameters)
                        {
                            parameterList.Add(new SqlParameter() { ParameterName = param.ParameterName, Value = dbCommand.Parameters[param.ParameterName].Value });
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);

                    }
                    finally
                    {
                        dbCommand.Dispose();
                        dbConnection.Close();
                    }
                }
            }
            SqlParameter[] parameters = parameterList.ToArray();
            return parameters;
        }

        //Can insert bulk list to database by open connection once. this method accept only storedprocedure with many in parameters and only one output parameter
        public static string ExecuteNonQueryBulkInsertion(string procName, SqlParameter[] inParameters, SqlParameter outParameter)
        {


            List<SqlParameter> parameterList = new List<SqlParameter>();
            SqlParameter outputParam;
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                using (SqlCommand sqlCommand = new SqlCommand(procName))
                {
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Connection = sqlConnection;

                    outputParam = sqlCommand.Parameters.Add(outParameter.ParameterName, outParameter.SqlDbType, outParameter.Size);
                    outputParam.Direction = ParameterDirection.Output;

                    foreach (var param in inParameters)
                    {
                        //Database.AddInParameter(sqlCommand, param.ParameterName, param.DbType, param.Value);
                        sqlCommand.Parameters.AddWithValue(param.ParameterName, param.Value);
                    }
                    //sqlCommand.Parameters.AddWithValue("@DTQuestions", bulkDT);
                    try
                    {
                        Debug.WriteLine("Executing ExecuteNonQueryBulkInsertion -> " + procName);
                        sqlConnection.Open();
                        sqlCommand.ExecuteNonQuery();

                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);

                    }
                    finally
                    {
                        sqlCommand.Dispose();
                        sqlConnection.Close();
                    }
                }
            }
            return outputParam.Value.ToString();
        }

    }
}
