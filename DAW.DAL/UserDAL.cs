﻿using DAW.Entites;
using DAW.Service;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAW.DAL
{
    public class UserDAL : IUserService
    {
        #region CreateNewUser
        public int CreateNewUser(UserEntities user)
        {
            int result = 0;

            var inParameter = new[]
            {
                new SqlParameter () { ParameterName="@firstname", Value= user.Firstname}
                ,new SqlParameter () { ParameterName="@lastname", Value= user.Lastname}
                ,new SqlParameter () { ParameterName="@dateofbirth", Value= user.Dateofbirth}
                ,new SqlParameter () { ParameterName="@address", Value= user.Address}
                ,new SqlParameter () { ParameterName="@address2", Value= user.Address2}
                ,new SqlParameter () { ParameterName="@phone1", Value= user.Phone1}
                ,new SqlParameter () { ParameterName="@phone2", Value= user.Phone2}
                ,new SqlParameter () { ParameterName="@email1", Value= user.Email1}
                ,new SqlParameter () { ParameterName="@email2", Value= user.Email2}
                ,new SqlParameter () { ParameterName="@systemid", Value= user.Systemid}
                ,new SqlParameter () { ParameterName="@weightkg", Value= user.Weightkg}
                ,new SqlParameter () { ParameterName="@imageurl", Value= user.Imageurl}
                ,new SqlParameter () { ParameterName="@lastweightedby", Value= user.Lastweightedby}
                ,new SqlParameter () { ParameterName="@lastweighteddate", Value= user.Lastweighteddate}
                ,new SqlParameter () { ParameterName="@active", Value= user.Active}
                ,new SqlParameter () { ParameterName="@activedate", Value= user.Activedate}
                ,new SqlParameter () { ParameterName="@inactivedate", Value= user.Inactivedate}
                ,new SqlParameter () { ParameterName="@createdby", Value= user.Createdby}
                ,new SqlParameter () { ParameterName="@createddate", Value= user.Createddate}
                ,new SqlParameter () { ParameterName="@lastupdateby", Value= user.Lastupdateby}
                ,new SqlParameter () { ParameterName="@lastupdatedate", Value= user.Lastupdatedate}
            };
            var outParameter = new[] {
                new SqlParameter(){ParameterName="@UserId", SqlDbType=SqlDbType.Int}
            };

            var us = DatabaseDAL.QueryNonReaderOutputParameters("DAW_CreateNewUser", inParameter, outParameter);
            result = int.Parse(us[0].Value.ToString());
            return result;
        }
        #endregion

        #region UpdateUserById
        public int UpdateUserById(UserEntities user)
        {
            int result = 0;
            var inParameter = new[]
            {
                new SqlParameter(){ParameterName="@userid", Value=user.UserId}
                ,new SqlParameter () { ParameterName="@firstname", Value= user.Firstname}
                ,new SqlParameter () { ParameterName="@lastname", Value= user.Lastname}
                ,new SqlParameter () { ParameterName="@dateofbirth", Value= user.Dateofbirth}
                ,new SqlParameter () { ParameterName="@address", Value= user.Address}
                ,new SqlParameter () { ParameterName="@address2", Value= user.Address2}
                ,new SqlParameter () { ParameterName="@phone1", Value= user.Phone1}
                ,new SqlParameter () { ParameterName="@phone2", Value= user.Phone2}
                ,new SqlParameter () { ParameterName="@email1", Value= user.Email1}
                ,new SqlParameter () { ParameterName="@email2", Value= user.Email2}
                ,new SqlParameter () { ParameterName="@systemid", Value= user.Systemid}
                ,new SqlParameter () { ParameterName="@weightkg", Value= user.Weightkg}
                ,new SqlParameter () { ParameterName="@imageurl", Value= user.Imageurl}
                ,new SqlParameter () { ParameterName="@lastweightedby", Value= user.Lastweightedby}
                ,new SqlParameter () { ParameterName="@lastweighteddate", Value= user.Lastweighteddate}
                ,new SqlParameter () { ParameterName="@active", Value= user.Active}
                ,new SqlParameter () { ParameterName="@activedate", Value= user.Activedate}
                ,new SqlParameter () { ParameterName="@inactivedate", Value= user.Inactivedate}
                ,new SqlParameter () { ParameterName="@createdby", Value= user.Createdby}
                ,new SqlParameter () { ParameterName="@createddate", Value= user.Createddate}
                ,new SqlParameter () { ParameterName="@lastupdateby", Value= user.Lastupdateby}
                ,new SqlParameter () { ParameterName="@lastupdatedate", Value= user.Lastupdatedate}
            };
            var outParameter = new[] {
                new SqlParameter(){ParameterName="@AffectedRow", SqlDbType=SqlDbType.Int}
            };

            var us = DatabaseDAL.QueryNonReaderOutputParameters("DAW_UpdateUserById", inParameter, outParameter);
            result = int.Parse(us[0].Value.ToString());
            return result;
        }
        #endregion

        #region  GetUserById
        public UserEntities GetUserById(int userid, int systemid) {
            var inParameter = new[]
            {
                new SqlParameter () { ParameterName="@userid", Value= userid}               
                ,new SqlParameter () { ParameterName="@systemid", Value= systemid}
            };
            UserEntities user = new UserEntities();
            using (IDataReader reader = DatabaseDAL.QueryReader("DAW_GetUserById", true, inParameter))
            {
                if(reader.Read())
                {
                    user = GetUserFromReader(reader);
                }
                reader.Dispose();

            }
            return user;
        }
        #endregion
        private UserEntities GetUserFromReader(IDataReader reader)
        {
            UserEntities user = new UserEntities();

            user.UserId = reader["userid"] == DBNull.Value ? 0 : int.Parse(reader["userid"].ToString());
            user.Firstname = reader["firstname"] == DBNull.Value ? string.Empty : reader["firstname"].ToString();
            user.Lastname = reader["lastname"] == DBNull.Value ? string.Empty : reader["lastname"].ToString();
            user.Dateofbirth = reader["dateofbirth"] == DBNull.Value ? DateTime.Parse("01/01/1900") : DateTime.Parse(reader["dateofbirth"].ToString());
            user.Address = reader["address"] == DBNull.Value ? string.Empty : reader["address"].ToString();
            user.Address2 = reader["address2"] == DBNull.Value ? string.Empty : reader["address2"].ToString();
            user.Phone1 = reader["phone1"] == DBNull.Value ? string.Empty : reader["phone1"].ToString();
            user.Phone2 = reader["phone2"] == DBNull.Value ? string.Empty : reader["phone2"].ToString();
            user.Email1 = reader["email1"] == DBNull.Value ? string.Empty : reader["email1"].ToString();
            user.Email2 = reader["email2"] == DBNull.Value ? string.Empty : reader["email2"].ToString();
            user.Systemid = reader["systemid"] == DBNull.Value ? 0 : int.Parse(reader["systemid"].ToString());
            user.Weightkg = reader["weightkg"] == DBNull.Value ? 0.0m : decimal.Parse(reader["weightkg"].ToString());
            user.Imageurl = reader["imageurl"] == DBNull.Value ? "~/images/ProfilePictures/default.jpg" : reader["imageurl"].ToString();
            user.Lastweightedby = reader["lastweightedby"] == DBNull.Value ? string.Empty : reader["lastweightedby"].ToString();
            user.Lastweighteddate = reader["lastweighteddate"] == DBNull.Value ? DateTime.Parse("01/01/1900") : DateTime.Parse(reader["lastweighteddate"].ToString());
            user.Active = reader["active"] != DBNull.Value && bool.Parse(reader["active"].ToString());
            user.Activedate = reader["activedate"] == DBNull.Value ? DateTime.Parse("01/01/1900") : DateTime.Parse(reader["activedate"].ToString());
            user.Inactivedate = reader["inactivedate"] == DBNull.Value ? DateTime.Parse("01/01/1900") : DateTime.Parse(reader["inactivedate"].ToString());
            user.Createdby = reader["createdby"] == DBNull.Value ? 0 : int.Parse(reader["createdby"].ToString());
            user.Createddate = reader["createddate"] == DBNull.Value ? DateTime.Parse("01/01/1900") : DateTime.Parse(reader["createddate"].ToString());
            user.Lastupdateby = reader["lastupdateby"] == DBNull.Value ? 0 : int.Parse(reader["lastupdateby"].ToString());
            user.Lastupdatedate = reader["lastupdatedate"] == DBNull.Value ? DateTime.Parse("01/01/1900") : DateTime.Parse(reader["lastupdatedate"].ToString());

            return user;
        }
        #region DeleteUserById
        public int DeleteUserById(int userid, int systemid)
        {
            int result = 0;
            var inParameter = new[]
           {
                new SqlParameter () { ParameterName="@userid", Value= userid}
                ,new SqlParameter () { ParameterName="@systemid", Value= systemid}
            };
            var outParameter = new[] {
                new SqlParameter(){ParameterName="@AffectedRow", SqlDbType=SqlDbType.Int}
            };
            var us = DatabaseDAL.QueryNonReaderOutputParameters("DAW_DeleteUserById", inParameter, outParameter);
            result = int.Parse(us[0].Value.ToString());
            return result;
        }
        #endregion

        #region GetUserBySystemId
        public List<UserEntities> GetUsersBySystemId(int systemid)
        {
            var result = new List<UserEntities>();

            var inParameter = new[] {
                new SqlParameter(){ParameterName="@SystemId", Value=systemid}
            };

            using (IDataReader reader = DatabaseDAL.QueryReader("DAW_GeAllUsersBySystemId", true, inParameter))
            {
                while(reader.Read())
                {
                    result.Add(GetUserFromReader(reader));
                }
                reader.Dispose();
            }
            if (result.Count == 0) result = null;

           return result;
        }
        #endregion
    }
}
