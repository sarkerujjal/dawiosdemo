﻿using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;



namespace DAW.DAL
{
    public static class DAWAppConfiguration
    {
        public static Database SQLAppConnectionManager
        {
            get
            {
                var db = EnterpriseLibraryContainer.Current.GetInstance<Database>("DAWConnection");
                try
                {
                    if (db == null) throw new ArgumentNullException(string.Format("Connection{0}String", "ConnectionString configuration is missing"));
                    {
                        return EnterpriseLibraryContainer.Current.GetInstance<Database>("DAWConnection");
                    }
                }
                catch
                {
                    throw (new NullReferenceException("ConnectionString configuration is missing. Please check your web.config file."));
                }
            }

        }
    }
}
